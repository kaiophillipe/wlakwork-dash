progressTitle = document.getElementById('progressTitle')

function progressSteps(param) {
  progressDisplays(param)
}

// Adicione no for mais etapas conforme necessário 
function progressDisplays(param) {
  for(x=4;x>0;x--) {
    document.getElementById('progress'+x).style.display = 'none';
    document.getElementById('countProgress'+x).classList.remove('active');
    document.getElementById('mBcountProgress'+x).classList.remove('active');
  }  
  for(x=1;x<=param;x++) {
    document.getElementById('countProgress'+x).classList.add('active');
    document.getElementById('mBcountProgress'+x).classList.add('active');
  }  
  document.getElementById('progress'+param).style.display = "block"  

  switch(param) {
    case 1:
      progressTitle.innerHTML = 'Escolha o Seu Pacote';
    break;
    case 2:
      progressTitle.innerHTML = 'Informação de Faturação';
    break;
    case 3:
      progressTitle.innerHTML = 'Pagamento';
    break;
    case 4:
      progressTitle.innerHTML = 'Confirmar';
    break;
  }
}
//Chama a funçao a primeira vez para começar no estado 1
progressDisplays(1)