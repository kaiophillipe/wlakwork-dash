posTitle = document.getElementById("positionTitle");
locat = document.getElementById("location");
jobOff = document.getElementById("jobOffer");

rEmailR = document.getElementById("receiveEmailRadio");
rEmail = document.getElementById("receiveEmail");

rExtFormR = document.getElementById("receiveExternalFormRadio");
rExtForm = document.getElementById("receiveExternalForm");

rTelR = document.getElementById("receiveTelRadio");
rTel = document.getElementById("receiveTel");

function progressSteps(param) {
  if(param > 1) {
    if(posTitle.value == ''){
      alert('O campo (Título da Posição *) é obrigatório!');
    } else if(locat.value == ''){
      alert('O campo (Localidade *) é obrigatório!');
    } else if(jobOff.value == ''){
      alert('O campo (Sua oferta de emprego *) é obrigatório!');
    } else if(rEmail.value == '' && rExtForm.value == '' && rTel.value == ''){
      alert('O tópico (Como receber candidaturas) é obrigatório uma entrada!');
    } else {
        progressDisplays(param)
    }   
  } else {
      progressDisplays(param)
  }
}

// Adicione no for mais etapas conforme necessário 
function progressDisplays(param) {
  for(x=5;x>0;x--) {
    document.getElementById('progress'+x).style.display = 'none';
    document.getElementById('countProgress'+x).classList.remove('active');
    document.getElementById('mBcountProgress'+x).classList.remove('active');
  }  
  for(x=1;x<=param;x++) {
    document.getElementById('countProgress'+x).classList.add('active');
    document.getElementById('mBcountProgress'+x).classList.add('active');
  }  
  document.getElementById('progress'+param).style.display = "block"  

  switch(param) {
    case 1:
      progressTitle.innerHTML = 'Detalhes do Emprego';
    break;
    case 2:
      progressTitle.innerHTML = 'Pré-Visualização';
    break;
    case 3:
      progressTitle.innerHTML = 'Informação de Faturação';
    break;
    case 4:
      progressTitle.innerHTML = 'Pagamento';
    break;
    case 5:
      progressTitle.innerHTML = 'Confirmar';
    break;
  }
}
//Chama a funçao a primeira vez para começar no estado 1
progressDisplays(1)


function changeSelect(param) {
  document.getElementById(param).style.color = "#2D2730";
}

rEmail.addEventListener("focus", function () {
  radios("receiveEmailRadio");
  clearInputs("rEmail");
});
rExtForm.addEventListener("focus", function () {
  radios("receiveExternalFormRadio");
  clearInputs("rExtForm");
});
rTel.addEventListener("focus", function () {
  radios("receiveTelRadio");
  clearInputs("rTel");
});

function radios(param) {
  rEmailR.checked = false;
  rExtFormR.checked = false;
  rTelR.checked = false;
  document.getElementById(param).checked = true;
  clearInputs(param);
}

function clearInputs(param) {
  if (param != "receiveEmailRadio" && param != "rEmail") {
    rEmail.value = "";
  }
  if (param != "receiveExternalFormRadio" && param != "rExtForm") {
    rExtForm.value = "";
  }
  if (param != "receiveTelRadio" && param != "rTel") {
    rTel.value = "";
  }
}
