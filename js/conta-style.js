title = document.getElementById("title");
nameUser = document.getElementById("nameUser");
nickname = document.getElementById("nickname");
yourPosition = document.getElementById("yourPosition");
companyName = document.getElementById("companyName");
email = document.getElementById("email");
password = document.getElementById("password");
passwordConfirm = document.getElementById("passwordConfirm");

function senAccount() {
    if(title.value == ''){
        alert('O campo (Título *) é obrigatório!');
    } else if(nameUser.value == ''){
        alert('O campo (Nome *) é obrigatório!');
    } else if(nickname.value == ''){
        alert('O campo (Apelido *) é obrigatório!');    
    } else if(companyName.value == ''){
        alert('O campo (Nome da Empresa *) é obrigatório!');
    } else if(email.value == ''){
        alert('O campo (E-mail *) é obrigatório!');   
    } else if(password.value != passwordConfirm.value){
        alert('As senhas para alteração não conferem!');
    } else {
        fetch(`php/senAccount.php?title=${title}&nameUser=${nameUser}&nickname=${nickname}&yourPosition=${yourPosition}&companyName=${companyName}&email=${email}&password=${password}&passwordConfirm=${passwordConfirm}`)
        .then(response => response.text())
        .then(content => {
            //ex.: mensagem enviada!
            alert(content);                                          
        })
    }
}

function changeSelect(param) {
    document.getElementById(param).style.color = "#2D2730";
  }