companyName = document.getElementById("companyName");
adress = document.getElementById("adress");
adress2 = document.getElementById("adress2");
postalCode = document.getElementById("postalCode");
city = document.getElementById("city");
country = document.getElementById("country");
IVAnumber = document.getElementById("IVAnumber");
tel = document.getElementById("tel");

function sendBilling() {
    if(companyName.value == ''){
        alert('O campo (Nome da Empresa *) é obrigatório!');
    } else if(adress.value == ''){
        alert('O campo (Morada *) é obrigatório!');
    } else if(postalCode.value == ''){
        alert('O campo (Código postal *) é obrigatório!');    
    } else if(city.value == ''){
        alert('O campo (Cidade *) é obrigatório!');
    } else if(country.value == ''){
        alert('O campo (País *) é obrigatório!');   
    } else if(tel.value == ''){
        alert('O campo (Número do telefone *) é obrigatório!');
    } else {
        fetch(`php/senAccount.php?companyName=${companyName}&adress=${adress}&adress2=${adress2}&postalCode=${postalCode}&city=${city}&country=${country}&IVAnumber=${IVAnumber}&tel=${tel}`)
        .then(response => response.text())
        .then(content => {
            //ex.: faturação atualizada!
            alert(content);                                          
        })
    }
}

function changeSelect(param) {
    document.getElementById(param).style.color = "#2D2730";
  }